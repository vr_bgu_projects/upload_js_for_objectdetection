const Express = require('express');
const multer = require('multer');
const bodyParser = require('body-parser');
const app = Express();
// var PythonShell = require('python-shell');
const fs = require('fs');
const log = require('./logger')(module);
const util = require('util');
const morgan = require('morgan');
const path = require('path');
const os = require('os');
const fsExtra = require('fs-extra');
const StringBuilder = require("string-builder");
const serveIndex = require('serve-index');
const shell = require('shelljs');
const {execFile, exec} = require('child_process');
//const youtubedl = require('youtube-dl');
const ytdl = require('ytdl-core');
const https = require('https');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// create a write stream (in append mode)
const accessLogStream = fs.createWriteStream(path.join(__dirname, '/logs/access.log'), {flags: 'a'});
// setup the logger
app.use(morgan('combined', {stream: accessLogStream, immediate: true}));

/* CONSTANTS */
const IMAGES_FOLDER = "./Images";
const RESULTS_FOLDER = "./results";
const DARKNET_PATH = process.cwd();
const IMAGE_EXT_LIST = ["jpg", "jpeg", "png", "gif", "tiff", "bmp"];
const VIDEOS_EXT_LIST = ["mp4"];
const MOVIE_DEFAULT_FILE_NAME = 'movie.mp4';
const DARKNET_WINDOWS_CONSOLE = "darknet_console.exe";
const DARKNET_LINUX_PREFIX = "darknet detector test cfg/voc.data cfg/tiny-yolo-voc.cfg tiny-yolo-voc.weights";
const DARKNET_WINDOWS_MOVIE_COMMAND = "yolo_console_video.exe";
const DARKNET_WINDOWS_IMAGE_COMMAND = "darknet_console.exe";
const APP_URL = 'http://52.166.104.198:3000';

app.get("/", function (req, res) {
  res.sendFile(__dirname + "/index.html");
});

app.get("/api/createFolder/:folder", function (req, res) {
  const folderName = req.params.folder;
  if (folderName) {
    const newDir = createImagesFolder(folderName);
    res.end(newDir + ' created');
  }
});

app.get("/api/getImageResultLinks/:folder", function (req, res) {
  const folderName = req.params.folder;
  if (folderName) {
    let folderResultsPath = path.join(RESULTS_FOLDER, folderName);
    if (isFolderExists(folderResultsPath)){
      const imagesArray = [];

      new Promise(resolve => {

        var promise =fs.readdirSync(folderResultsPath).forEach(file => {
          const extension = getFileExtension(file); // remove dot
          if (!IMAGE_EXT_LIST.includes(extension)) {
            return; // stop processing this iteration
          }
          const imagePath = path.join(RESULTS_FOLDER, folderName, file);
          //Creating txt file with images path
          let imgName = path.basename(file);
          let url = `${APP_URL}/results/${folderName}/${imgName}`;
          imagesArray.push(imagePath);
        });
        resolve(imagesArray);
      }).then(result => {
        res.json(result);

      });
    }
    else{
      res.end('folder doesn\'t exists: ' + folderName);
    }

  }
});

app.get("/api/createYoloShell/:folder", function (req, res) {
  const folderName = req.params.folder;
  const imagesFolderPath = path.join(IMAGES_FOLDER, folderName);
  const imagesFolderPathPhysical = path.join(__dirname, imagesFolderPath);

  try {
    if (folderName && imagesFolderPath && isFolderExists(imagesFolderPathPhysical)) {
      const sb = new StringBuilder();
      //log.info(`OS: ${os.platform()}`);
      //let isWin = (os.platform() === 'win32');
      if (!isOSWin()) {
        sb.append(`#!/bin/bash\ncd ${process.cwd()}\n`);
      }
      fs.readdirSync(imagesFolderPath).forEach(file => {
        //log.debug(file);
        const extension = path.extname(file).replace(/^\.+|\.+$/g, ''); // remove dot
        if (!IMAGE_EXT_LIST.includes(extension)) {
          return; // stop processing this iteration
        }

        const imagePath = path.join(process.cwd(), imagesFolderPath, file);
        if (!isOSWin()) {
          sb.appendFormat("{0} {1} -out results/{2}/{3}\n",
              darknetLinuxPrefix,
              imagePath,
              folderName,
              path.parse(file).name.trim());
        }
        else {
          sb.appendFormat("darknet.exe detector test cfg/combine9k.data yolo9000.cfg yolo9000.weights {0} -out results/{1}/{2}",
              imagePath,
              folderName,
              path.parse(file).name.trim());
          sb.appendLine();
        }
      });

      const shellFile = getShellFilePath(folderName);
      createResultsFolder(folderName);

      if (fs.existsSync(shellFile)) {
        fs.unlinkSync(shellFile);
        writeFile(shellFile, sb).then(() => {
          var isShellExists = checkIfFileExists(shellFile);
          if (isShellExists) {
            res.end(shellFile + ' created');
          }
        });
      }
      else {
        writeFile(shellFile, sb).then((result) => {
          var isShellExists = checkIfFileExists(shellFile);
          if (isShellExists) {
            res.end(shellFile + ' created');
          }
        });
      }
    }
  }
  catch (e) {
    log.error(e);
    res.end('Something went wrong: ' + e);
  }
});

app.get("/api/createYoloCMD/:folder", function (req, res) {
  let responseResult = '';
  const folderName = req.params.folder;
  const imagesFolderPath = path.join(IMAGES_FOLDER, folderName);
  const imagesFolderPathPhysical = path.join(__dirname, imagesFolderPath);

  try {
    if (folderName && imagesFolderPath && isFolderExists(imagesFolderPathPhysical)) {
      createResultsFolder(folderName);

      //Create CMD file
      const shellFile = getShellFilePath(folderName);

      writeImagesListFile(folderName).then((imagesListFilePath) => {
        if (checkIfFileExists(imagesListFilePath)) {
          responseResult += `${imagesListFilePath} created\n`;

          const txtFileName = path.basename(imagesListFilePath);
          let windowsDarknetCommand = `${DARKNET_WINDOWS_CONSOLE} ${txtFileName} results/${folderName}`;
          let isShellExists = checkIfFileExists(shellFile);
          if (isShellExists) {
            fs.unlinkSync(shellFile);

            writeFile(shellFile, windowsDarknetCommand).then(() => {
              isShellExists = checkIfFileExists(shellFile);
              if (isShellExists) {
                //log.info(shellFile + ' created');
                responseResult += shellFile + ' created';
              }
              res.end(responseResult);
            });
          }
          else {
            writeFile(shellFile, windowsDarknetCommand).then(() => {
              isShellExists = checkIfFileExists(shellFile);
              if (isShellExists) {
                //log.info(shellFile + ' created');
                responseResult += shellFile + ' created';
              }
            });
            res.end(responseResult);
          }
        }

        else {
          res.end("ImagesList File was not created: " + '\n' + imagesListFilePath);

        }

      });
    }
    else {
      res.end('Issue with folder/folderPath');
    }
  }
  catch (e) {
    log.error(e);
    res.end('Something went wrong: ' + e);
  }
});

app.get("/api/isFolderExists/:folder", function (req, res) {
  let result = false;
  const folderName = req.params.folder;
  if (folderName) {
    const folderPath = path.join(IMAGES_FOLDER, folderName);
    if (fs.existsSync(folderPath)) {
      const shellFile = getShellFilePath(folderName);
      if (fs.existsSync(shellFile)) {
        result = true;
      }
      else {
        result = false;
      }
    }
    else {
      result = false;
    }
  }
  else {
    result = false;
  }
  res.json({result});
});

app.get("/api/clearFolder/:folder", function (req, res) {
  let result = false;
  const folderName = req.params.folder;
  if (folderName) {
    const folderPath = path.join(__dirname, IMAGES_FOLDER, folderName);
    if (fs.existsSync(folderPath)) {
      clearFolderContent(folderPath).then(() => {
        checkIfFolderEmpty(folderPath).then(
            function (result) {
              res.json({result});
            }, function (err) {
              log.error(err);
            }
        );
      });
    }
  }
  else {
    result = false;
  }
});

app.get("/api/clearResult/:folder", function (req, res) {
  let result = false;
  const folderName = req.params.folder;
  if (folderName) {
    const folderPath = path.join(__dirname, RESULTS_FOLDER, folderName);
    if (fs.existsSync(folderPath)) {
      clearFolderContent(folderPath).then(() => {
        checkIfFolderEmpty(folderPath).then(
            function (result) {
              res.json({result});
            }, function (err) {
              log.error(err);
            }
        );
      });
    }
  }
  else {
    result = false;
  }
});

app.post("/api/Upload", function (req, res) {
  //log.info(JSON.stringify(req.params));
  //log.info(JSON.stringify(req.query));
  //log.info(req.body) // populated!

  //const filename = req.file.filename;

  upload(req, res, function (err) {
    //console.log(JSON.stringify(req.body));
    //log.info(JSON.stringify(req.query));
    if (err || !req.file) {
      log.error(err);
      return res.end("Something went wrong! " + err);
    }
    //RunObjectDetection(filename);
    //var originalFileName = req.file.originalname
    //console.log(originalFileName)
    try {
      let extension = getFileExtension(req.file.originalname);

      if (req.query.dir) {
        let fileDest = '';
        if (IMAGE_EXT_LIST.includes(extension)) { //If image - save real name
          fileDest = path.join(req.file.destination, '/' + req.query.dir + '/', req.file.originalname);
        }
        else if (VIDEOS_EXT_LIST.includes(extension)) { //If this video save as movie.mp4
          //fileDest = path.join(req.file.destination, '/' + req.query.dir + '/', MOVIE_DEFAULT_FILE_NAME);
          fileDest = path.join(req.file.destination, '/' + req.query.dir + '/', req.file.originalname);
        }
        else {
          fileDest = path.join(req.file.destination, '/' + req.query.dir + '/', req.file.originalname);
        }
        fsExtra.moveSync(req.file.path, fileDest, {overwrite: true}, function (err) {
          if (err) {
            log.error(err);
            console.error(err);
            res.end(err);
          }
        });
        if (checkIfFileExists(fileDest)) {
          return res.end(`File (${req.file.path}) uploaded successfully!. This path: ${fileDest}`);
        }
        else {
          return res.end(`File (${req.file.path}) cannot be found on server!. This path: ${fileDest}`);
        }
      }
      else {
        return res.end(`File (${req.file.path}) uploaded successfully! to the Images folder.`);
      }
    } catch (e) {
      log.error(e);
      res.end(e);
    }
  });
});

app.get("/api/isImageExists/:folder/:imgName", function (req, res) {
  let result = false;
  const folderName = req.params.folder;
  const imgName = req.params.imgName;
  result = isImageExistsByFolderAndImageName(folderName, imgName, IMAGES_FOLDER);
  res.json({result});
});

app.get("/api/isImageResultExists/:folder/:imgName", function (req, res) {
  let result = false;
  const folderName = req.params.folder;
  const imgName = req.params.imgName;

  result = isImageExistsByFolderAndImageName(folderName, imgName, RESULTS_FOLDER);
  res.json({result});
});

app.get("/api/getImageResultLink/:folder/:imgName", function (req, res) {
  let result = false;
  const folderName = req.params.folder;
  const imgName = req.params.imgName;

  result = isImageExistsByFolderAndImageName(folderName, imgName, RESULTS_FOLDER);
  if (result) {
    res.json({url: `${APP_URL}/results/${folderName}/${imgName}`});
  }
  else {
    res.json({});
  }
});

app.get("/api/isShellExists/:folder", function (req, res) {
  try {
    let result = false;
    const folderName = req.params.folder;

    if (folderName) {
      const shellPath = getShellFilePath(folderName);
      if (fs.existsSync(shellPath)) {
        result = true;
      }
      else {
        result = false;
      }
    }
    else {
      result = false;
    }
  }
  catch (e) {
    log.error(e);
    console.log("Error:\n" + e);
  }
  res.json({result});
});

app.get("/api/runShell/:folder", function (req, res) {
  const folderName = req.params.folder;
  if (folderName) {
    try {
      const shellPath = getShellFilePath(folderName);
      if (!fs.existsSync(shellPath)) {
        let errorMessage = `Shell file: ${shellPath} doesn't exists`;
        log.error(errorMessage);
        res.end(errorMessage);
      }
      shell.exec(shellPath);
      res.end(`shell ${shellPath} is running...`);
    }
    catch (e) {
      log.error(e);
      res.end('Something went wrong: ' + e);
    }
  }
  res.end('Something went wrong');
});

app.get("/api/runCmd/:folder", function (req, res) {
  const folderName = req.params.folder;
  if (folderName) {
    try {
      const shellPath = getShellFilePath(folderName);
      if (!fs.existsSync(shellPath)) {
        let errorMessage = `CMD file: ${shellPath} doesn't exists`;
        log.error(errorMessage);
        res.end(errorMessage);
      }
      shell.exec(shellPath);
      res.end(`CMD ${shellPath} is running...`);
    }
    catch (e) {
      log.error(e);
      res.end('Something went wrong: ' + e);
    }
  }
  res.end('Something went wrong');
});

app.get("/api/processMovie/:folder/:movieName", function (req, res) {
  const folderName = req.params.folder;
  let movieName = req.params.movieName;
  let result = {};

  if (folderName && movieName) {
    try {
      movieName = replaceLast(movieName, '$', '.');

      //const movieFilePath = path.join(__dirname, IMAGES_FOLDER, folderName, MOVIE_DEFAULT_FILE_NAME);
      let movieFilePath = path.join(__dirname, IMAGES_FOLDER, folderName, movieName);

      let resultsFolder = createResultsFolder(folderName);
      log.info(`results folder created: ${resultsFolder}`);
      if (!checkIfFileExists(movieFilePath)) {
        res.end(`file: ${movieFilePath} doesn't exists`);
      }

      // exec(DARKNET_WINDOWS_MOVIE_COMMAND,
      //     [movieFilePath, 'results/' + folderName],


      // write to a new file named 2pac.txt
      let batFileName = 'z_' + movieName + '.bat';

       //"C:\Apps\upload_js_for_objectdetection\Images\NARCOS_Season_3_Trailer_2017_Netflix\movie.mp4" %resultsFolder%
      let content = `cd "%~dp0"\nyolo_console_video.exe ${movieFilePath} results/${folderName}`;
      fs.writeFile(batFileName, content, (err) => {
        // throws an error, you could also catch it here
        if (err) throw err;

        // success case, the file was saved
        console.log('content saved!');
        let movieNameResult = replaceLast(movieName, '.mp4', '.avi');
        let resultMovie = `${APP_URL}/results/${folderName}/${movieNameResult}`;
        result.url = resultMovie;
        exec(batFileName,
            (error, stdout, stderr) => {
              if (error) {
                console.error(`exec error: ${error}`);
                return;
              }
              console.log(`stdout: ${stdout}`);
              console.log(`stderr: ${stderr}`);

              result.stdout = stdout;
              result.stderr = stderr;
              result.date = getDateTime();
              res.json(result);
            });

      });
    }
    catch (e) {
      log.error(e);
      result.error = 'Something went wrong: ' + e;
      res.json(result);
    }
  }
  else {
    result.error = `folder name ${folderName} does not exists`;
    res.json(result);
  }
});

app.get("/api/processImage/:folder/:imgName", function (req, res) {
  const folderName = req.params.folder;
  let imgName = req.params.imgName;
  let result = {};
  if (folderName && imgName) {
    imgName = replaceLast(imgName, '$', '.');
    try {
      const imgFilePath = path.join(__dirname, IMAGES_FOLDER, folderName, imgName);
      let resultsFolder = createResultsFolder(folderName);
      log.info(`results folder created: ${resultsFolder}`);
      if (!checkIfFileExists(imgFilePath)) {
        res.end(`file: ${imgFilePath} doesn't exists`);
      }
      log.info(`Image file path: ${imgFilePath}`);
      execFile(`${DARKNET_WINDOWS_IMAGE_COMMAND}`,
          [imgFilePath, 'results/' + folderName],
          //{cwd: DARKNET_PATH  },
          (error, stdout, stderr) => {
            if (error) {
              let errorMessage = `exec error: ${error}`;
              log.error(errorMessage);
              console.error(errorMessage);
              result.error = `exec error: ${error}`;
              res.json(result);
              return;
            }
            console.log(`stdout: ${stdout}`);
            log.info(`stdout: ${stdout}`);

            console.log(`stderr: ${stderr}`);
            log.info(`stderr: ${stderr}`);

            let resultImage = `${APP_URL}/results/${folderName}/${imgName}`;
            result.stdout = stdout;
            result.stderr = stderr;
            result.url = resultImage;
            result.date = getDateTime();
            res.json(result);
          });
    }
    catch (e) {
      log.error(e);
      result.error = 'Something went wrong: ' + e;
      res.json(result);
    }
  }
  else {
    result.error = `folder name ${folderName} does not exists`;
    res.json(result);
  }
});

app.get("/api/processFolder/:folder", function (req, res) {
  let responseResult = '';
  const folderName = req.params.folder;
  const imagesFolderPath = path.join(IMAGES_FOLDER, folderName);
  const imagesFolderPathPhysical = path.join(__dirname, imagesFolderPath);

  try {
    if (folderName && imagesFolderPath && isFolderExists(imagesFolderPathPhysical)) {
      createResultsFolder(folderName);
      writeImagesListFile(folderName).then((imagesListFilePath) => {
        if (checkIfFileExists(imagesListFilePath)) {
          responseResult += `${imagesListFilePath} created\n`;
          const txtFileName = path.basename(imagesListFilePath);
          execFile(`${DARKNET_WINDOWS_IMAGE_COMMAND}`,
              [imagesListFilePath, 'results/' + folderName],
              //{cwd: DARKNET_PATH  },
              (error, stdout, stderr) => {
                if (error) {
                  log.error(`exec error: ${error}`);
                  console.error(`exec error: ${error}`);
                  res.end(`exec error: ${error}`);
                  return;
                }
                console.log(`stdout: ${stdout}`);
                log.info(`stdout: ${stdout}`);

                console.log(`stderr: ${stderr}`);
                log.info(`stderr: ${stderr}`);

                let folderResults = `${APP_URL}/results/${folderName}`;
                responseResult += stdout + '\n' + folderResults
                res.end(responseResult);
              });
        }
        else {
          res.end("ImagesList File was not created: " + '\n' + imagesListFilePath);
        }
      });
    }
    else {
      res.end('Issue with folder/folderPath');
    }
  }
  catch (e) {
    log.error(e);
    res.end('Something went wrong: ' + e);
  }
});

app.get("/api/youtube/download/:folder/:key", function (req, res) {
  let responseResult = '';
  const folderName = req.params.folder;
  const ytKey = req.params.key;
  const imagesFolderPath = path.join(IMAGES_FOLDER, folderName);
  const imagesFolderPathPhysical = path.join(__dirname, imagesFolderPath);

  try {
    if (ytKey && folderName && imagesFolderPath) {

      const newDir = createImagesFolder(folderName);
      createResultsFolder(folderName);

      const savedMoviePath = path.join(imagesFolderPath, 'movie.mp4');

      ytdl('http://www.youtube.com/watch?v=' + ytKey,  { filter: (format) => format.container === 'mp4' })
        .pipe(fs.createWriteStream(savedMoviePath));


      // var video = youtubedl('http://www.youtube.com/watch?v=' + ytKey,
      //   // Optional arguments passed to youtube-dl.
      //   ['--format=18'],
      //   // Additional options can be given for calling `child_process.execFile()`.
      //   { cwd: __dirname });
      //
      // // Will be called when the download starts.
      // video.on('info', function(info) {
      //   console.log('Download started');
      //   console.log('filename: ' + info.filename);
      //   console.log('size: ' + info.size);
      // });
      //
      // video.pipe(fs.createWriteStream('movie.mp4'));
    }
    else {
      res.end('Something went wrong');
    }
  }
  catch (e) {
    log.error(e);
    res.end('Something went wrong: ' + e);
  }
});

app.get("/api/youtube/downloadCaption/:folder/:key", function (req, res) {
  let responseResult = '';
  const folderName = req.params.folder;
  const ytKey = req.params.key;
  const imagesFolderPath = path.join(IMAGES_FOLDER, folderName);
  const imagesFolderPathPhysical = path.join(__dirname, imagesFolderPath);

  try {
    if (ytKey && folderName && imagesFolderPath && isFolderExists(imagesFolderPathPhysical)) {

      const newDir = createImagesFolder(folderName);
      createResultsFolder(folderName);

      const id    = 'https://www.youtube.com/watch?v=' + ytKey;
      const lang  = 'en';

      ytdl.getInfo(id, (err, info) => {
        if (err) throw err;
        var tracks = info
          .player_response.captions
          .playerCaptionsTracklistRenderer.captionTracks;
        if (tracks && tracks.length) {
          console.log('Found captions for',
            tracks.map(t => t.name.simpleText).join(', '));
          var track = tracks.find(t => t.languageCode === lang);
          if (track) {
            console.log('Retrieving captions:', track.name.simpleText);
            console.log('URL', track.baseUrl);
            var output = `yt.caption.xml`;

            const savedMovieCaptionPath = path.join(imagesFolderPath, output);

            console.log('Saving to', output);
            https.get(track.baseUrl, (res) => {
              res.pipe(fs.createWriteStream(path.resolve(savedMovieCaptionPath)));
            });

          } else {
            console.log('Could not find captions for', lang);

          }
        } else {
          console.log('No captions found for this video');
        }

      });
    }
    else {
      res.end('Something went wrong');
    }
  }
  catch (e) {
    log.error(e);
    res.end('Something went wrong: ' + e);
  }
});

app.get("/api/youtube/getCaption/:folder", function (req, res) {
  const folderName = req.params.folder;
  const imagesFolderPath = path.join(IMAGES_FOLDER, folderName);
  const imagesFolderPathPhysical = path.join(__dirname, imagesFolderPath);

  try {
    if (folderName && imagesFolderPath && isFolderExists(imagesFolderPathPhysical)) {

      const filepath = path.resolve(imagesFolderPath, 'yt.caption.xml');

      if (checkIfFileExists(filepath))
      {
        fs.readFile(filepath, "utf8", function(err, data){
          if(err) throw err;
          res.send(data);
        });
      }

      else{
        res.end ('There is no info file for the this movie: ' + folderName);
      }
    }
    else {
      res.end('Something went wrong');
    }
  }
  catch (e) {
    log.error(e);
    res.end('Something went wrong: ' + e);
  }
});

app.get("/api/youtube/downloadInfo/:folder/:key", function (req, res) {
  let responseResult = '';
  const folderName = req.params.folder;
  const ytKey = req.params.key;
  const imagesFolderPath = path.join(IMAGES_FOLDER, folderName);
  const imagesFolderPathPhysical = path.join(__dirname, imagesFolderPath);

  try {
    if (ytKey && folderName && imagesFolderPath && isFolderExists(imagesFolderPathPhysical)) {


      const newDir = createImagesFolder(folderName);
      createResultsFolder(folderName);

      const filepath = path.resolve(imagesFolderPath, 'yt.info.json');

      if (checkIfFileExists((filepath))){
        res.end(`Youtube (key ${ytKey}) json info already exists: ${filepath}`);
      }

      ytdl.getInfo(ytKey, (err, info) => {
        if (err) throw err;
        console.log('title:', info.title);
        console.log('rating:', info.average_rating);
        console.log('uploaded by:', info.author.name);
        const json = JSON.stringify(info, null, 2)
          .replace(/(ip(?:=|%3D|\/))((?:(?:25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(?:25[0-5]|2[0-4]\d|[01]?\d\d?)|[0-9a-f]{1,4}(?:(?::|%3A)[0-9a-f]{1,4}){7})/ig, '$10.0.0.0');
        fs.writeFile(filepath, json, err => {
          if (err) throw err;
        });
      });
    }
    else {
      res.end('Something went wrong');
    }
  }
  catch (e) {
    log.error(e);
    res.end('Something went wrong: ' + e);
  }
});

app.get("/api/youtube/getInfo/:folder", function (req, res) {
  let responseResult = '';
  const folderName = req.params.folder;
  const imagesFolderPath = path.join(IMAGES_FOLDER, folderName);
  const imagesFolderPathPhysical = path.join(__dirname, imagesFolderPath);

  try {
    if (folderName && imagesFolderPath && isFolderExists(imagesFolderPathPhysical)) {

      const filepath = path.resolve(imagesFolderPath, 'yt.info.json');

      if (checkIfFileExists(filepath))
      {
        fs.readFile(filepath, "utf8", function(err, data){
          if(err) throw err;
          res.json(JSON.parse(data));
        });
      }

      else{
        res.end ('There is no info file for the this movie: ' + folderName);
      }
    }
    else {
      res.end('Something went wrong');
    }
  }
  catch (e) {
    log.error(e);
    res.end('Something went wrong: ' + e);
  }
});

app.get("/api/youtube/downloadRange/:folder/:key/:start/:end", function (req, res) {
  let responseResult = '';
  const folderName = req.params.folder;
  const ytKey = req.params.key;
  const startTime = req.params.start;
  const endTime = req.params.end;

  const imagesFolderPath = path.join(IMAGES_FOLDER, folderName);
  const imagesFolderPathPhysical = path.join(__dirname, imagesFolderPath);

  try {
    if (ytKey && folderName && imagesFolderPath) {

      const newDir = createImagesFolder(folderName);
      createResultsFolder(folderName);

      const url = 'https://www.youtube.com/watch?v=' + ytKey;
      const output = path.resolve(imagesFolderPath, `movie_s${startTime}_e${endTime}.mp4`);

      const video = ytdl(url, { range: { start: startTime, end: endTime } });
      video.pipe(fs.createWriteStream(output));

      video.on('end', () => {
        ytdl(url, { range: { start: endTime + 1 } })
          .pipe(fs.createWriteStream(output, { flags: 'a' }));

        res.json({url: `${APP_URL}/Images/${folderName}/${imgName}`});
      });
    }
    else {
      res.end('Something went wrong');
    }
  }
  catch (e) {
    log.error(e);
    res.end('Something went wrong: ' + e);
  }
});

app.get("/log", function (req, res) {
  const filePath = process.cwd() + '/logs/all.log';
  fs.readFile(filePath, 'utf8', function (err, content) {
    res.send(content);
  });
});

app.get("/accessLog", function (req, res) {
  const filePath = process.cwd() + '/logs/access.log';
  fs.readFile(filePath, 'utf8', function (err, content) {
    res.send(content);
  });
});

app.get("/clearLog", function (req, res) {
  const filePath = process.cwd() + '/logs/all.log';
  const filePath2 = process.cwd() + '/logs/access.log';

  fs.truncate(filePath, 0, function () {
    res.end('Log cleared')
  });

  fs.truncate(filePath2, 0, function () {
    res.end('Log cleared')
  });
});

app.use(Express.static('results'));

app.use(Express.static('Images'));

app.use('/images', serveIndex('Images', {'icons': true})); // shows you the file list

app.use('/results', serveIndex('results', {'icons': true})); // shows you the file list

app.get('/results/:folder/:imgName', function (req, res) {
  res.sendfile(path.join(__dirname, 'results', req.params.folder, req.params.imgName));
});

app.get('/images/:folder/:imgName', function (req, res) {
  res.sendfile(path.join(__dirname, 'images', req.params.folder, req.params.imgName));
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.status(404);
  log.debug('%s %d %s', req.method, res.statusCode, req.url);
  res.json({
    error: 'Not found'
  });
});

// error handlers
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  log.error('%s %d %s', req.method, res.statusCode, err.message);
  res.json({
    error: err.message
  });
});

let writeFile = function (file, data) {
  return new Promise(resolve => {
    fs.writeFile(file, data.toString(), {
      flag: 'wx',
      mode: 0o777
    }, function (err) {
      if (err) {
        log.error(err);
        throw err;
      }
      log.info(`${file} : It's saved!`);
      resolve(true);
    });
  });
};

function RunObjectDetection(file) {
  console.log(file);
  PythonShell.run('my_script.py', function (err) {
    if (err) {
      log.error(err);
      throw err;
    }
    console.log('finished');
  });
}

function createFolder(dirPath) {
  if (!fs.existsSync(dirPath)) {
    fs.mkdirSync(dirPath);
    return dirPath;
  }
  return dirPath;
}

function createImagesFolder(folderName) {
  const dirPath = path.join(IMAGES_FOLDER, folderName);
  createFolder(dirPath);
}

let clearFolderContent = function (folderPath) {

  return new Promise(resolve => {
    let files = fs.readdirSync(folderPath);

    for (const file of files) {
      fs.unlinkSync(path.join(folderPath, file), err => {
        if (err) {
          log.error(err);
          throw err;
        }
      });
    }
    resolve(true);
  });
};

function checkIfFileExists(file) {
  if (fs.existsSync(file)) {
    return true;
  }
  return false;
}

function replaceLast(x, y, z) {
  var a = x.split("");
  var length = y.length;
  if (x.lastIndexOf(y) != -1) {
    for (var i = x.lastIndexOf(y); i < x.lastIndexOf(y) + length; i++) {
      if (i == x.lastIndexOf(y)) {
        a[i] = z;
      }
      else {
        delete a[i];
      }
    }
  }

  return a.join("");
}

let isImageExistsByFolderAndImageName = function (folderName, imgName, baseFolder) {
  let result = false;
  if (folderName && imgName && baseFolder) {
    const folderPath = path.join(baseFolder, folderName);
    if (fs.existsSync(folderPath)) {
      const imageFile = path.join(folderPath, imgName);
      if (fs.existsSync(imageFile)) {
        result = true;
      }
      else {
        result = false;
      }
    }
    else {
      result = false;
    }
  }
  else {
    result = false;
  }
  return result;
};

const checkIfFolderEmpty = (folder) => {
  return new Promise(resolve => {
    let result = false;
    let files = fs.readdirSync(folder);
    log.info('line 325 files' + files);
    if (!files.length) {
      log.info('line 327 files' + files);
      result = true;
    }
    else {
      result = false;
    }
    log.info('line 332 files' + files);

    resolve(result);
  });
};

let createResultsFolder = function (folderName) {
  let resultsFolder = path.join(RESULTS_FOLDER, folderName);
  //Creating the results folder where we we'll save the results
  return createFolder(resultsFolder);
};

function getDateTime() {

  var date = new Date();

  var hour = date.getHours();
  hour = (hour < 10 ? "0" : "") + hour;

  var min = date.getMinutes();
  min = (min < 10 ? "0" : "") + min;

  var sec = date.getSeconds();
  sec = (sec < 10 ? "0" : "") + sec;

  var year = date.getFullYear();

  var month = date.getMonth() + 1;
  month = (month < 10 ? "0" : "") + month;

  var day = date.getDate();
  day = (day < 10 ? "0" : "") + day;

  return year + "/" + month + "/" + day + " " + hour + ":" + min + ":" + sec;

}

let getFileExtension = function (file) {
  return path.extname(file).replace(/^\.+|\.+$/g, '');
};
let writeImagesListFile = function (folderName) {
  return new Promise(resolve => {

    const txtFilePath = getImagesListFilePath(folderName);

    const imagesFolderPath = path.join(IMAGES_FOLDER, folderName);
    if (folderName && imagesFolderPath) {
      //Build Image List StringBuilder
      const sb = new StringBuilder();
      fs.readdirSync(imagesFolderPath).forEach(file => {
        const extension = getFileExtension(file); // remove dot
        if (!IMAGE_EXT_LIST.includes(extension)) {
          return; // stop processing this iteration
        }
        const imagePath = path.join(process.cwd(), imagesFolderPath, file);
        //Creating txt file with images path
        sb.appendFormat("{0}", imagePath);
        sb.appendLine();
      });

      const txtFileContent = sb.toString();
      if (fs.existsSync(txtFilePath)) {
        fs.unlinkSync(txtFilePath);
        let promise = writeFile(txtFilePath, txtFileContent).then(() => {
          var isTxtFileExists = checkIfFileExists(txtFilePath);
          if (isTxtFileExists) {
            return txtFilePath;
          }
        });
        promise.then(function (value) {
          resolve(value);
        });
      }
      else {
        let promise = writeFile(txtFilePath, txtFileContent).then(() => {
          var isTxtFileExists = checkIfFileExists(txtFilePath);
          if (isTxtFileExists) {
            return txtFilePath;
          }
        });

        promise.then(function (value) {
          resolve(value);
        });
      }
    }
  });
};

let isFolderExists = (folderPath) => {
  // fs.stat(folderPath, function (err, stats) {
  //   if (err) {
  //     // Directory doesn't exist or something.
  //     callback(false);
  //   }
  //   if (!stats.isDirectory()) {
  //     // This isn't a directory!
  //     console.log('temp is not a directory!');
  //     callback(false);
  //   } else {
  //     console.log('Does exist');
  //     callback(true);
  //   }
  // });

  try {
    return fs.statSync(folderPath).isDirectory();
  } catch (e) {
    if (e.code === 'ENOENT') {
      console.log('Folder doesn\'t exist');
      return false;
    } else {
      throw e;
    }
  }
};

function getStorage() {
  try {
    const Storage = multer.diskStorage({
      destination: function (req, file, callback) {
        callback(null, IMAGES_FOLDER);
      },

      filename: function (req, file, callback) {
        callback(null, file.originalname);
      }
    });
    return Storage;
  } catch (ex) {
    log.error(ex);
    console.log("Error :\n" + ex);
  }
}

const upload = getMulter();

function getMulter() {
  return multer({
    storage: getStorage()
  }).single("uploader", 3); //Field name and max count
}

let getShellFilePath = (folderName) => {
  let shellPath = '';

  if (!isOSWin()) { //It's Linux
    shellPath = path.join(DARKNET_PATH, 'z_' + folderName + '.sh');
  }
  else { //It's windows
    shellPath = path.join(DARKNET_PATH, 'z_' + folderName + '.cmd');
  }
  return shellPath;
};

let getImagesListFilePath = (folderName) => {
  let txtPath = path.join(DARKNET_PATH, 'z_' + folderName + '.txt');
  return txtPath;
};

let isOSWin = () => {
  try {
    let isWin = (os.platform() === 'win32');
    return isWin;
  }
  catch (e) {
    logger.error(e);
  }
};

app.listen(3000, function (a) {
  console.log("Listening to port 3000");
});